package de.mkonersmann.simplecommands;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.mkonersmann.simplecommands.model.Command;
import de.mkonersmann.simplecommands.model.Model;
import de.mkonersmann.simplecommands.model.Result;
import de.mkonersmann.simplecommands.model.State;
import de.mkonersmann.simplecommands.repository.ICommandRepository;
import de.mkonersmann.simplecommands.repository.InMemoryRepository;
import de.mkonersmann.simplecommands.scheduler.IScheduler;
import de.mkonersmann.simplecommands.scheduler.ThreadPoolScheduler;

public class CommandManager implements IScheduler {
	protected ICommandRepository repository;
	protected IScheduler scheduler;

	protected Model model = new Model();

	static final Logger log = LoggerFactory.getLogger(CommandManager.class.getName());

	public CommandManager(ICommandRepository repository, IScheduler scheduler) {
		this.repository = Objects.requireNonNullElse(repository, new InMemoryRepository());
		log.info("Repository: Using " + repository.getClass().getSimpleName());
		this.scheduler = Objects.requireNonNullElse(scheduler, ThreadPoolScheduler.builder().build());
		log.info("Scheduler: Using " + scheduler.getClass().getSimpleName());
	}

	public void init() {
		model = repository.load();
		model.getCommands().stream().filter(c -> c.getState() == State.CREATED || c.getState() == State.SCHEDULED)
				.forEach(c -> {
					log.info("Rescheduling: " + c);
					scheduler.submit(c);
				});
	}

	public ICommandRepository getRepository() {
		return repository;
	}

	public IScheduler getScheduler() {
		return scheduler;
	}

	public Set<Command<?>> getCommands() {
		return model.getCommands();
	}

	public Optional<Command<?>> getCommand(String uuid) {
		return model.getCommand(uuid);
	}

	@Override
	public <T extends Result> CompletableFuture<Optional<T>> submit(Command<T> command) {
		Objects.requireNonNull(command);
		model.addCommand(command);
		repository.store(model);
		CompletableFuture<Optional<T>> submitted = scheduler.submit(command).thenApplyAsync(r -> {
			repository.store(model);
			return r;
		});
		return submitted;
	}
}
