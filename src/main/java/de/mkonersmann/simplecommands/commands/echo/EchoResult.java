package de.mkonersmann.simplecommands.commands.echo;

import de.mkonersmann.simplecommands.model.Result;

public class EchoResult extends Result {
	private static final long serialVersionUID = 8121441541719459731L;
	String echoed;

	public EchoResult(String echoed) {
		this.echoed = echoed;
	}

	public String getEchoed() {
		return echoed;
	}

	public void setEchoed(String echoed) {
		this.echoed = echoed;
	}
}
