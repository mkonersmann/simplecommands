package de.mkonersmann.simplecommands.commands.echo;

import java.io.PrintStream;
import java.util.Objects;
import java.util.Optional;

import de.mkonersmann.simplecommands.model.Command;

public class EchoCommand extends Command<EchoResult> {

	protected String message;
	protected PrintStream stream;

	public EchoCommand(PrintStream stream, String message) {
		this.stream = stream;
		this.message = message;
	}

	@Override
	public Optional<EchoResult> execute() {
		Objects.requireNonNull(stream);
		Objects.requireNonNull(message);

		logDebug("Echoing '%s'".formatted(message));
		stream.println(message);
		stream.flush();
		logDebug("Echoed '%s'".formatted(message));

		return Optional.of(new EchoResult(message));
	}

}
