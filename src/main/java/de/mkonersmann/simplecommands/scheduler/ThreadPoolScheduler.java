package de.mkonersmann.simplecommands.scheduler;

import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import de.mkonersmann.simplecommands.model.Command;
import de.mkonersmann.simplecommands.model.Result;

public class ThreadPoolScheduler extends AbstractScheduler {

	private ExecutorService executor;

	private ThreadPoolScheduler(Builder builder) {
		this.executor = builder.executor;

		BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(builder.queueCapacity);
		RejectedExecutionHandler handler = new ThreadPoolExecutor.CallerRunsPolicy();

		executor = new ThreadPoolExecutor(
				builder.corePoolSize,
				builder.maxPoolSize,
				builder.keepAliveTime,
				TimeUnit.SECONDS,
				workQueue,
				handler);
	}

	public static class Builder {
		private ExecutorService executor;
		private int corePoolSize = Runtime.getRuntime().availableProcessors();
		private int maxPoolSize = Runtime.getRuntime().availableProcessors()*4;
		private long keepAliveTime = 60L;
		private int queueCapacity = 1000;

		public Builder corePoolSize(int corePoolSize) {
			this.corePoolSize = corePoolSize;
			return this;
		}

		public Builder maxPoolSize(int maxPoolSize) {
			this.maxPoolSize = maxPoolSize;
			return this;
		}

		public Builder keepAliveTime(long keepAliveTime) {
			this.keepAliveTime = keepAliveTime;
			return this;
		}

		public Builder queueCapacity(int queueCapacity) {
			this.queueCapacity = queueCapacity;
			return this;
		}

		public ThreadPoolScheduler build() {
			return new ThreadPoolScheduler(this);
		}
	}

	@Override
	protected <T extends Result> CompletableFuture<Optional<T>> doSubmit(Command<T> command) {
		return CompletableFuture.supplyAsync(() -> command.execute_wrapper(), executor);
	}

	public static Builder builder() {
		return new Builder();
	}

}
