package de.mkonersmann.simplecommands.scheduler;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import de.mkonersmann.simplecommands.model.Command;
import de.mkonersmann.simplecommands.model.Result;
import de.mkonersmann.simplecommands.model.State;

public abstract class AbstractScheduler implements IScheduler {

	@Override
	public <T extends Result> CompletableFuture<Optional<T>> submit(Command<T> command) {
		command.setState(State.SCHEDULED);
		command.setSubmitted(ZonedDateTime.now());
		CompletableFuture<Optional<T>> result = doSubmit(command);

		result.thenApply(r -> {
			command.setFinished(ZonedDateTime.now());
			if (command.isFailed())
				command.setState(State.FAIL);
			else
				command.setState(State.SUCCESS);
			return r;
		}).exceptionally((t) -> {
			command.setFinished(ZonedDateTime.now());
			command.setThrowable(t.getCause());
			command.setState(State.ERROR);
			command.logError(t.getMessage(), t);
			return Optional.empty();
		});

		return result;
	}

	protected abstract <T extends Result> CompletableFuture<Optional<T>> doSubmit(Command<T> command);

}
