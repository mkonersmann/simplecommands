package de.mkonersmann.simplecommands.scheduler;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import de.mkonersmann.simplecommands.model.Command;
import de.mkonersmann.simplecommands.model.Result;

public interface IScheduler {
	public <T extends Result> CompletableFuture<Optional<T>> submit(Command<T> command);
}
