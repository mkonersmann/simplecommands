package de.mkonersmann.simplecommands.model;

/**
 * A result containing a String for your convenience.
 */
public class StringResult extends Result {

	private static final long serialVersionUID = -5326251927202824673L;
	private final String string;

	public StringResult(String string) {
		this.string = string;
	}

	public String getString() {
		return this.string;
	}
}
