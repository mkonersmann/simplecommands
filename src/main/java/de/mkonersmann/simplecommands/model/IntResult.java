package de.mkonersmann.simplecommands.model;

/**
 * A result containing an integer for your convenience.
 */
public class IntResult extends Result {
	private static final long serialVersionUID = 4281181644435047375L;
	private final int number;

	public IntResult(int number) {
		this.number = number;
	}

	public int getNumber() {
		return this.number;
	}
}
