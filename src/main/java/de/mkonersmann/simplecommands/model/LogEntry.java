package de.mkonersmann.simplecommands.model;

import java.time.ZonedDateTime;

public class LogEntry {
	private ZonedDateTime dateTime;
	protected String message;
	protected Throwable throwable;
	protected LogLevel loglevel;

	public LogEntry(String message, Throwable throwable, LogLevel loglevel, ZonedDateTime dateTime) {
		this.message = message;
		this.throwable = throwable;
		this.loglevel = loglevel;
		this.dateTime = dateTime;
	}

	public LogEntry(String message, Throwable throwable, LogLevel loglevel) {
		this.message = message;
		this.throwable = throwable;
		this.loglevel = loglevel;
		this.dateTime = ZonedDateTime.now();
	}

	public String getMessage() {
		return message;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public LogLevel getLoglevel() {
		return loglevel;
	}

	public ZonedDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(ZonedDateTime dateTime) {
		this.dateTime = dateTime;
	}

}
