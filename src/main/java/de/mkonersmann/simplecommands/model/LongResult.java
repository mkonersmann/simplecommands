package de.mkonersmann.simplecommands.model;

/**
 * A result containing a long for your convenience.
 */
public class LongResult extends Result {
	private static final long serialVersionUID = -192888247081513892L;
	private final long number;

	public LongResult(long number) {
		this.number = number;
	}

	public long getNumber() {
		return this.number;
	}
}
