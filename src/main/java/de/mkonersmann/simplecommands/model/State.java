package de.mkonersmann.simplecommands.model;

public enum State {
	/** Created, but not scheduled, yet **/ 
	CREATED,
	
	/** Scheduled, but not started, yet **/ 
	SCHEDULED,
	
	/** Currently in execution **/
	EXECUTING,
	
	/** Finished successful **/
	SUCCESS,
	
	/** Finished with a failure **/
	FAIL,
	
	/** Ended with an exception. Probably not finished, but will not run anymore **/
	ERROR
}
