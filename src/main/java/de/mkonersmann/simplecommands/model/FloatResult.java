package de.mkonersmann.simplecommands.model;

/**
 * A result containing a float for your convenience.
 */
public class FloatResult extends Result {

	private static final long serialVersionUID = 2402692890752109356L;
	private final float number;

	public FloatResult(float number) {
		this.number = number;
	}

	public float getNumber() {
		return this.number;
	}
}
