package de.mkonersmann.simplecommands.model;

/**
 * A result containing a boolean for your convenience.
 */
public class BooleanResult extends Result {
	private static final long serialVersionUID = -3433649209109492970L;
	final boolean isTrue;

	public BooleanResult(boolean isTrue) {
		this.isTrue = isTrue;
	}

	public boolean isTrue() {
		return this.isTrue;
	}

}
