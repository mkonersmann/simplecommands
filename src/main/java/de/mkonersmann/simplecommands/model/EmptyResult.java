package de.mkonersmann.simplecommands.model;

/**
 * An empty result for your convenience.
 */
public class EmptyResult extends Result {

	private static final long serialVersionUID = -7163302279407558802L;

}
