package de.mkonersmann.simplecommands.model;

public enum LogLevel {
	TRACE, INFO, WARN, ERROR, DEBUG
}
