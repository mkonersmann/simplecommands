package de.mkonersmann.simplecommands.model;

/**
 * A result containing a double for your convenience.
 */
public class DoubleResult extends Result {

	private static final long serialVersionUID = -238344928430633249L;
	private final double number;

	public DoubleResult(double number) {
		this.number = number;
	}

	public double getNumber() {
		return this.number;
	}
}
