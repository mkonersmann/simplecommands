package de.mkonersmann.simplecommands.model;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Command<T extends Result> {

	protected UUID uuid = UUID.randomUUID();
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	private T result = null;
	private Throwable throwable = null;
	private State state = State.CREATED;
	private boolean failed = false;
	private ZonedDateTime submitted, executed, finished;
	private List<LogEntry> logs = new LinkedList<>();

	public T getResult() {
		return this.result;
	}

	public State getState() {
		return this.state;
	}

	public void setState(State state) {
		this.state = state;
	}

	protected void fail() {
		this.failed = true;
	}

	public boolean isFailed() {
		return this.failed;
	};

	public boolean hasExceptionalResult() {
		return this.throwable != null;
	}

	public Throwable getThrowable() {
		return this.throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

	public Optional<T> execute_wrapper(){
		state = State.EXECUTING;
		return execute();
	};
	
	public abstract Optional<T> execute();

	public ZonedDateTime getSubmitted() {
		return this.submitted;
	}

	public void setSubmitted(ZonedDateTime submitted) {
		this.submitted = submitted;
	}

	public List<LogEntry> getLogs() {
		return this.logs;
	}

	public void log(LogEntry entry) {
		logs.add(entry);
		switch (entry.getLoglevel()) {
		case INFO:
			logger.info(entry.getMessage());
			break;
		case WARN:
			logger.warn(entry.getMessage());
			break;
		case ERROR:
			logger.error(entry.getMessage(), entry.getThrowable());
			break;
		case DEBUG:
			logger.debug(entry.getMessage());
			break;
		case TRACE:
			logger.trace(entry.getMessage());
			break;
		}
	}

	public void logInfo(String message) {
		log(new LogEntry(message, null, LogLevel.INFO));
	}

	public void logWarn(String message) {
		log(new LogEntry(message, null, LogLevel.WARN));
	}

	public void logError(String message, Throwable throwable) {
		log(new LogEntry(message, throwable, LogLevel.ERROR));
	}

	public void logError(String message) {
		log(new LogEntry(message, null, LogLevel.ERROR));
	}

	public void logDebug(String message) {
		log(new LogEntry(message, null, LogLevel.DEBUG));
	}

	public void logTrace(String message) {
		log(new LogEntry(message, null, LogLevel.TRACE));
	}

	@Override
	public String toString() {
		return "{\"state\":\"%s\",\"type\": \"%s\", \"throwable\": \"%s\", \"submitted\": \"%s\"}".formatted(
				this.getState().name(), this.getClass().getName(),
				this.getThrowable() != null ? this.getThrowable().getClass().getName() : "",
				this.getSubmitted().format(DateTimeFormatter.ISO_DATE_TIME));
	}

	public void setFinished(ZonedDateTime finished) {
		this.finished = finished;
	}

	public void setExecuted(ZonedDateTime executed) {
		this.executed = executed;
	}

	public ZonedDateTime getExecuted() {
		return executed;
	}

	public ZonedDateTime getFinished() {
		return finished;
	}

	public Duration getDuration() {
		return Duration.between(executed, finished);
	}

	public UUID getUuid() {
		return uuid;
	}
}
