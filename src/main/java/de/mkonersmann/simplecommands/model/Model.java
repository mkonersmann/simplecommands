package de.mkonersmann.simplecommands.model;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

public class Model {
	LinkedHashSet<Command<?>> commands = new LinkedHashSet<>();

	public Set<Command<?>> getCommands() {
		return commands;
	}

	public void addCommand(Command<?> command) {
		commands.add(command);
	}
	
	public Optional<Command<?>> getCommand(String uuid) {
		return commands.stream().filter(c -> c.getUuid().toString().equals(uuid)).findFirst();
	}
	
	@Override
	public String toString() {
		return """
				{"model":[
				%s
				]}
				""".formatted(
						commands.stream()
						.map(c -> c.toString() + System.lineSeparator())
						.reduce(String::concat)
						.get()
						);
	}
}
