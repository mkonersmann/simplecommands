package de.mkonersmann.simplecommands.repository;

import de.mkonersmann.simplecommands.model.Model;

/**
 * A very simple repository that stores the model in-memory. The repository is
 * not persistent and data will be lost when the application is shut down.
 */
public class InMemoryRepository implements ICommandRepository {

	Model model = new Model();
	
	@Override
	public synchronized void store(Model model) {
		this.model = model;
	}

	@Override
	public Model load() {
		return model;
	}
}
