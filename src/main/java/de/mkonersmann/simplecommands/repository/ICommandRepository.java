package de.mkonersmann.simplecommands.repository;

import de.mkonersmann.simplecommands.model.Model;

public interface ICommandRepository {
	public void store(Model model);
	public Model load();
}
