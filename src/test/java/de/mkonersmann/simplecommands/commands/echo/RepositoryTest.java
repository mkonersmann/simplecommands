package de.mkonersmann.simplecommands.commands.echo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.junit.jupiter.api.Test;

import de.mkonersmann.simplecommands.CommandManager;
import de.mkonersmann.simplecommands.model.Model;
import de.mkonersmann.simplecommands.repository.InMemoryRepository;
import de.mkonersmann.simplecommands.scheduler.ThreadPoolScheduler;

public class RepositoryTest {
	@Test
	public void repo() throws SQLException, InterruptedException, ExecutionException {
		TestEchoCommand cmd = new TestEchoCommand(System.out, "This should be printed.");

		InMemoryRepository repo = new InMemoryRepository();
		CommandManager manager = new CommandManager(repo, ThreadPoolScheduler.builder().build());
		manager.init();

		CompletableFuture<Optional<TestEchoResult>> future = manager.submit(cmd);
		future.get();
		Model loaded = repo.load();
		assertEquals(manager.getCommands().size(), loaded.getCommands().size());

	}
}
