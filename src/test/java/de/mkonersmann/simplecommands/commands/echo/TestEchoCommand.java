package de.mkonersmann.simplecommands.commands.echo;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.mkonersmann.simplecommands.model.Command;

public class TestEchoCommand extends Command<TestEchoResult> implements Serializable {

	private static final long serialVersionUID = -4526646757447138898L;
	static final Logger log = LoggerFactory.getLogger(TestEchoCommand.class.getName());

	protected String message;
	protected transient PrintStream stream;

	public TestEchoCommand(PrintStream stream, String message) {
		this.stream = stream;
		this.message = message;
	}

	@Override
	public Optional<TestEchoResult> execute() {
		Objects.requireNonNull(message);

		log.info(Thread.currentThread().getName());

		if ("Malicous message".equals(message)) {
			super.fail();
			return Optional.empty();
		}

		if ("This should **not** be printed.".equals(message)) {
			throw new IllegalArgumentException("Test Exception");
		}

		if (stream == null) {
			stream = System.out;
		}
		stream.println(message);
		stream.flush();

		return Optional.of(new TestEchoResult(message));
	}

	@Override
	public int hashCode() {
		return Objects.hash(message, stream);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TestEchoCommand other = (TestEchoCommand) obj;
		return Objects.equals(message, other.message);
	}

}