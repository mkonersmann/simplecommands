package de.mkonersmann.simplecommands.commands.echo;

import de.mkonersmann.simplecommands.model.Result;

public class TestEchoResult extends Result {

	private static final long serialVersionUID = 556865849131454005L;
	private String message;

	public TestEchoResult(String message) {
		this.setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
