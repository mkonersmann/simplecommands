package de.mkonersmann.simplecommands.commands.echo;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.LinkedList;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;

import org.junit.jupiter.api.Test;

import de.mkonersmann.simplecommands.CommandManager;
import de.mkonersmann.simplecommands.model.Command;
import de.mkonersmann.simplecommands.model.Model;
import de.mkonersmann.simplecommands.model.State;
import de.mkonersmann.simplecommands.repository.ICommandRepository;
import de.mkonersmann.simplecommands.scheduler.ThreadPoolScheduler;

public class EchoTest {

	@Test
	void success() throws InterruptedException, ExecutionException {
		TestEchoCommand cmd = new TestEchoCommand(System.out, "This should be printed.");

		CommandManager manager = new CommandManager(new TestRepo(), ThreadPoolScheduler.builder().build());

		manager.init();

		Optional<TestEchoResult> result = manager.submit(cmd).get();
		assertTrue(result.isPresent());
		assertEquals("This should be printed.", result.get().getMessage());
		assertEquals(State.SUCCESS, cmd.getState());
	}

	@Test
	void throwRuntimeException() throws InterruptedException, ExecutionException {
		TestEchoCommand cmd = new TestEchoCommand(null, "This should **not** be printed.");

		CommandManager manager = new CommandManager(new TestRepo(), ThreadPoolScheduler.builder().build());

		manager.init();

		manager.submit(cmd).exceptionallyAsync(e -> {
			assertEquals(CompletionException.class, e.getClass());
			assertNotNull(cmd.getThrowable());
			assertEquals(IllegalArgumentException.class, cmd.getThrowable().getClass());
			return (Optional<TestEchoResult>) null;
		}).get();
		assertEquals(State.ERROR, cmd.getState());
	}

	@Test
	void doFail() throws InterruptedException, ExecutionException {
		TestEchoCommand cmd = new TestEchoCommand(System.out, "Malicous message");

		CommandManager manager = new CommandManager(new TestRepo(), ThreadPoolScheduler.builder().build());

		manager.init();

		Optional<TestEchoResult> result = manager.submit(cmd).get();
		
		assertEquals(State.FAIL, cmd.getState());
		assertTrue(result.isEmpty());
	}

	@Test
	void echoMultipleTimes() throws InterruptedException, ExecutionException {
		LinkedList<TestEchoCommand> cmds = new LinkedList<>();
		int MAX = 10_000;
		for (int i = 0; i < MAX; i++)
			cmds.add(new TestEchoCommand(System.out, i + " - This should be printed."));

		CommandManager manager = new CommandManager(
				new TestRepo(),
				ThreadPoolScheduler.builder()
				.queueCapacity(MAX)
				.build());
		manager.init();

		LinkedList<CompletableFuture<Optional<TestEchoResult>>> cfs = new LinkedList<>();
		for (TestEchoCommand cmd : cmds)
			cfs.add(manager.submit(cmd));

		CompletableFuture.allOf(cfs.toArray(new CompletableFuture[cmds.size()])).get();
		for (Command<?> cmd : cmds)
			assertEquals(State.SUCCESS, cmd.getState());

	}

	class TestRepo implements ICommandRepository {

		private Model model = new Model();

		@Override
		public void store(Model model) {
			this.model = model;
		}

		@Override
		public Model load() {
			return model;
		}
	}

}
